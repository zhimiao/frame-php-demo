<?php
/**
 * 配置文件
 * Created by PhpStorm.
 * User: 倒霉狐狸
 * Date: 2018-11-27
 * Time: 09:54
 */

return [
	'is_dev' => true,
	'db' => [
		'dsn' => 'mysql:host=localhost;dbname=java2',
		'user' => 'root',
		'password' => 'root'
	],
	'redis' => [
		'port'=> '6379', // 端口号
		'host' => 'localhost',
		'auth' => 'password',
	],
	'cfg_map' => [
		'db' => 'database',
		'redis' => 'redis'
	],
];